import React from 'react';
import './NoticationPopup.scss';
import {truncateString} from '../../helpers/utils.js';

const NotificationPopup = ({notification, settings}) => {
  const {
    firstName,
    city,
    country,
    productName,
    timestamp,
    productImage
  } = notification;
  const {position, hideTimeAgo, truncateProductName} = settings;
  // Avada-SP__Postion--left
  return (
    <div
      className={`Avava-SP__Wrapper Avada-SP__Postion--${position} fadeInUp animated`}
    >
      <div className="Avava-SP__Inner">
        <div className="Avava-SP__Container">
          <a href="#" className={'Avava-SP__LinkWrapper'}>
            <div
              className="Avava-SP__Image"
              style={{
                backgroundImage: `url(${productImage})`
              }}
            />
            <div className="Avada-SP__Content">
              <div className={'Avada-SP__Title'}>
                {firstName} in {city}, {country}
              </div>
              <div className={'Avada-SP__Subtitle'}>
                purchased{' '}
                {truncateProductName
                  ? truncateString(productName, 16)
                  : productName}
              </div>
              <div className={'Avada-SP__Footer'}>
                {hideTimeAgo ? '' : `${timestamp}`}{' '}
                <span className="uni-blue">
                  <i className="fa fa-check" aria-hidden="true" /> by Avada
                </span>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {};

export default NotificationPopup;
