import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import NotificationPopup from '../components/NotificationPopup/NotificationPopup';
import {delay} from '../helpers/utils.js';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }

  async initialize({notifications, settings}) {
    this.insertContainer();
    if (this.checkUrlPermission(settings)) {
      await this.showNotifications({settings, notifications});
    }
  }

  async showNotifications({settings, notifications}) {
    await delay(settings.firstDelay);
    for (const notification of notifications) {
      this.display({notification: notification, settings: settings});
      await delay(settings.displayDuration);
      this.fadeOut();
      await delay(settings.popsInterval);
    }
  }

  checkUrlPermission(settings) {
    const includedUrls = settings?.includedUrls?.split('\n') || [];
    const excludedUrls = settings?.excludedUrls?.split('\n') || [];
    const allowShow = settings.allowShow;
    if (allowShow == 'all' && excludedUrls?.includes(window.location.href)) {
      return false;
    }
    if (
      allowShow == 'specific' &&
      !includedUrls?.includes(window.location.href)
    ) {
      return false;
    }

    return true;
  }

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');
    container.innerHTML = '';
  }

  display({notification, settings}) {
    const container = document.querySelector('#Avada-SalePop');
    render(
      <NotificationPopup notification={notification} settings={settings} />,
      container
    );
  }

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }
}
