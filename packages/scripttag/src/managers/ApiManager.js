import makeRequest from '../helpers/api/makeRequest';

export default class ApiManager {
  getNotifications = async () => {
    return this.getApiData();
  };

  getApiData = async () => {
    const apiUrl = `https://localhost:3000/clientApi/notifications?shopifyDomain=${Shopify.shop}`;
    const {notifications, settings} = await makeRequest(apiUrl, 'GET');

    return {notifications, settings};
  };
}
