import React from 'react';
import { Card, TextField, FormLayout, RangeSlider } from '@shopify/polaris';

export default function RangeInput({ lable, min, max, name, inputValue, helpText, handleChangeTiming, suffix }) {
  const suffixStyles = {
    maxWidth: "140px",
    textAlign: "right",
  };

  return (
    <RangeSlider
      output
      label={lable}
      min={min}
      max={max}
      value={inputValue}
      onChange={(value) => handleChangeTiming(name, value)}
      suffix={
        <div style={suffixStyles}>
          {
            <TextField
              type="number"
              value={String(inputValue)}
              onChange={(value) => handleChangeTiming(name, parseInt(value))}
              suffix={suffix}
            />
          }
        </div>}
      helpText={helpText}
    />
  )
}
