import React from 'react';
import PropTypes from 'prop-types';
import { Labelled, Stack, TextStyle, Card } from '@shopify/polaris';

const DesktopPositionInput = ({ label, helpText, value, onChange }) => {
  const options = [
    { label: 'Bottom left', value: 'bottom-left' },
    { label: 'Bottom right', value: 'bottom-right' },
    { label: 'Top left', value: 'top-left' },
    { label: 'Top right', value: 'top-right' }
  ]

  return (
    <Card.Section>
      <Labelled label={label} >
        <Stack>
          {options.map((option, key) => (
            <div
              key={key}
              className={`Avada-DesktopPosition ${value === option.value ? 'Avada-DesktopPosition--selected' : ''}`}
              onClick={() => onChange(option.value)}
            >
              <div
                className={`Avada-DesktopPosition__Input Avada-DesktopPosition__Input--${option.value}`}
              ></div>
            </div>
          ))}
        </Stack>
        <TextStyle variation="subdued">{helpText}</TextStyle>
      </Labelled>
    </Card.Section>
  );
};

DesktopPositionInput.propTypes = {
  label: PropTypes.string,
  options: PropTypes.array,
  value: PropTypes.string,
  onChange: PropTypes.func,
  helpText: PropTypes.string
};

export default DesktopPositionInput;
