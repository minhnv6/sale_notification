import React, { useState, useCallback } from 'react';
import { Page, Card, Tabs, Layout, Checkbox, TextField, Select, Button, FormLayout, Spinner } from '@shopify/polaris';
import { NotificationPopup } from '../../components/NotificationPopup/NotificationPopup';
import DesktopPositionInput from '../../components/settingComponents/DesktopPositionInput';
import useFetchApi from '../../hooks/api/useFetchApi';
import { api } from '../../helpers';
import { options } from '../../hooks/optionsValue';
import RangeInput from '../../components/settingComponents/RangeInput';

export default function Settings() {
  const [selected, setSelected] = useState(0);
  const [saveLoading, setSaveLoading] = useState(false);

  const { loading, data: input, setData: setInput } = useFetchApi('/settings')

  const handleChangeInput = (key, value) => {
    setInput(prev => ({
      ...prev,
      [key]: value
    }))
  }

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  const handleOnsubmit = async () => {
    setSaveLoading(true);
    await api('/settings', 'put', input)
    setSaveLoading(false);
  }

  const tabs = [
    {
      id: "Display",
      content: "Display",
      accessibilityLabel: "APPEARANCE",
      panelID: "all-customers-fitted-content-2",
      contentBody:
        <FormLayout>
          <FormLayout.Group>
            <DesktopPositionInput
              label="Desktop Position"
              helpText="The display position of the pop on your website."
              value={input.position}
              onChange={(val) => handleChangeInput('position', val)}
            />
          </FormLayout.Group>

          <FormLayout>
            <Card.Section>
              <Checkbox
                label="Hide time ago"
                checked={input.hideTimeAgo}
                onChange={(val) => handleChangeInput('hideTimeAgo', val)}
              />
              <Checkbox
                label="Truncate content text"
                checked={input.truncateProductName}
                onChange={(val) => handleChangeInput('truncateProductName', val)}
                helpText="If your product name is long for one line, it will be trullcated to 'Product na...'"
              />
            </Card.Section>
          </FormLayout>

          <Card.Section title="TIMING" flush>
            <FormLayout.Group>
              <RangeInput
                lable='Display duration'
                min={0}
                max={40}
                name='displayDuration'
                inputValue={input.displayDuration}
                helpText="How long each pop will display on your page."
                handleChangeTiming={handleChangeInput}
                suffix="Second(s)"
              />
              <RangeInput
                lable="Time before the fisrt pop"
                min={0}
                max={60}
                name='firstDelay'
                inputValue={input.firstDelay}
                helpText="The delay time before the fisrt notification."
                handleChangeTiming={handleChangeInput}
                suffix="Second(s)"
              />
            </FormLayout.Group>
            <FormLayout.Group>
              <RangeInput
                lable='Gap time betwent two pops'
                min={0}
                max={30}
                name='popsInterval'
                inputValue={input.popsInterval}
                helpText="The time interval betwent two popup notification."
                handleChangeTiming={handleChangeInput}
                suffix="Second(s)"
              />
              <RangeInput
                lable="Maximum of popups"
                min={0}
                max={80}
                name='maxPopsDisplay'
                inputValue={input.maxPopsDisplay}
                helpText="The maximum number of popups are allowed to show after page loading."
                handleChangeTiming={handleChangeInput}
                suffix="Pop(s)"
              />
            </FormLayout.Group>
          </Card.Section>
        </FormLayout>
    },
    {
      id: "Triggers",
      content: "Triggers",
      accessibilityLabel: "PAGES RESTICTION",
      panelID: "accepts-marketing-fitted-Ccontent-2",
      contentBody:
        <React.Fragment>
          <Card.Section>
            <Select
              options={options}
              onChange={(value) => handleChangeInput('allowShow', value)}
              value={input.allowShow}
            />
          </Card.Section>

          {
            input.allowShow === "specific" &&
            <Card.Section>
              <TextField
                label="Include Pages"
                value={input.includedUrls}
                onChange={(value) => handleChangeInput('includedUrls', value)}
                autoComplete="off"
                multiline={3}
                helpText="Page URls to show the pop-up (separated by new inlines)"
              />
            </Card.Section>
          }

          <Card.Section>
            <TextField
              disabled={input.allowShow === "specific"}
              label="Excluded Pages"
              value={input.excludedUrls}
              onChange={(value) => handleChangeInput('excludedUrls', value)}
              autoComplete="off"
              multiline={3}
              helpText="Page URls NOT to show the pop-up (separated by new inlines)"
            />
          </Card.Section>
        </React.Fragment>
    },
  ];

  return (
    <Page
      title="Settings"
      subtitle="Decide how your notifications will display"
      fullWidth
      primaryAction={
        <Button
          primary
          onClick={handleOnsubmit}
          loading={saveLoading}
        >
          Save
        </Button>
      }
    >
      <Layout>
        <Layout.Section secondary>
          <NotificationPopup
            settings={{ hideTimeAgo: input.hideTimeAgo, truncateProductName: input.truncateProductName }}
            firstName='John Doe'
            city='New York'
            country='United States'
            productName='Puffer Jacket With Hidden Hood'
            productImage='https://target.scene7.com/is/image/Target/GUEST_693c1197-393c-4aa5-a4e6-70ec71be5419?wid=315&hei=315&qlt=60&fmt=pjpeg'
            timestamp={new Date()}
          />
        </Layout.Section>
        <Layout.Section>
          <Card footerActionAlignment="left">
            <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} fitted>
              <Card.Section title={tabs[selected].accessibilityLabel} fullWidth>
                {loading ? <Spinner accessibilityLabel="Spinner example" size="large" /> : tabs[selected].contentBody}
              </Card.Section>
            </Tabs>
          </Card>
        </Layout.Section>
      </Layout>
    </Page >
  );
}
