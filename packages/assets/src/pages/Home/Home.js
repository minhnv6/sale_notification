import React, { useState, useCallback } from 'react';
import { Layout, Page, SettingToggle, TextStyle, Card } from '@shopify/polaris';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {

  const [active, setActive] = useState(false);

  const handleToggle = useCallback(() => setActive((active) => !active), []);

  const contentStatus = active ? "Disabled" : "Enabled";
  const textStatus = active ? "Enabled" : "Disabled";

  return (
    <Page
      title="Home"
      fullWidth
    >
      <Layout>
        <Layout.Section>
          {/* <Card> */}
          <SettingToggle
            action={{
              content: contentStatus,
              onAction: handleToggle,
            }}
            enabled={active}
          >
            App status is <TextStyle variation="strong">{textStatus}</TextStyle>.
          </SettingToggle>
          {/* </Card> */}
        </Layout.Section>
      </Layout>
    </Page>
  );
}
