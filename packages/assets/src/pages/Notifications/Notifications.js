import React, { useState } from 'react';
import { Layout, Page, ResourceList, Card, ResourceItem, Stack, Pagination } from '@shopify/polaris';
import { NotificationPopup } from '../../components/NotificationPopup/NotificationPopup';
import useFetchApi from '../../hooks/api/useFetchApi';
import moment from 'moment';

export default function Notifications() {
  const [selectedItems, setSelectedItems] = useState([]);
  const [sortValue, setSortValue] = useState("DATE_MODIFIED_DESC");
  const [pagenation, setPagenation] = useState({
    currentPage: 1,
    notificationPerPage: 5
  });

  const { loading, data: items} = useFetchApi(`/notifications`)
  const { data: setting } = useFetchApi('/settings')

  const resourceName = {
    singular: "notification",
    plural: "notifications",
  };

  const prevPage = () => {
    if (pagenation.currentPage > 1) {
      setPagenation({ ...pagenation, currentPage: pagenation.currentPage - 1 });
    }
  }

  const nextPage = () => {
    if (pagenation.currentPage < Math.ceil(items.length / pagenation.notificationPerPage)) {
      setPagenation({ ...pagenation, currentPage: pagenation.currentPage + 1 });
    }
  }

  return (
    <Page
      title="Notifications"
      subtitle="List of sales notification from Shoppify"
      fullWidth
    >
      <Layout>
        <Layout.Section>
          <Card>
            <ResourceList
              loading={loading}
              resourceName={resourceName}
              items={items.slice(
                pagenation.currentPage * pagenation.notificationPerPage - pagenation.notificationPerPage,
                pagenation.currentPage * pagenation.notificationPerPage
              )}
              renderItem={renderItem}
              selectedItems={selectedItems}
              onSelectionChange={setSelectedItems}
              selectable
              sortValue={sortValue}
              sortOptions={[
                { label: "Newest update", value: "DATE_MODIFIED_DESC" },
                { label: "Oldest update", value: "DATE_MODIFIED_ASC" },
              ]}
              onSortChange={(selected) => {
                setSortValue(selected);
                console.log(`Sort option changed to ${selected}.`);
              }}
            />
          </Card>
        </Layout.Section>

        <Layout.Section>
          <Pagination
            hasPrevious
            onPrevious={prevPage}
            hasNext
            onNext={nextPage}
          />
        </Layout.Section>

      </Layout>
    </Page>
  );

  function renderItem(item) {
    const { id, firstName, city, country, productName, timestamp, productImage } = item;
    return (
      <ResourceItem id={id} >
        <Stack distribution="fill">
          <NotificationPopup
            settings={setting}
            firstName={firstName}
            city={city}
            country={country}
            productName={productName}
            productImage={productImage}
            timestamp={new Date(timestamp._seconds * 1000 + timestamp._nanoseconds / 100000)}
          />
          <div style={{ textAlign: "right" }}>{`${moment(new Date(timestamp._seconds * 1000 + timestamp._nanoseconds / 100000)).fromNow()}`}{' '}</div>
        </Stack>
      </ResourceItem>
    );
  }
}
