import Router from 'koa-router';
import * as WebhookController from '../controllers/webhookController';
import { verifyWebhook } from '@avada/shopify-auth';

const router = new Router({
  prefix: '/webhook'
});

router.use(verifyWebhook());
router.post('/order/new', WebhookController.listenNewOrder);

export default router;
