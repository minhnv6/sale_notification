import Router from 'koa-router';
import * as settingController from '../controllers/settingController';
import * as notificationController from '../controllers/notificationController'
import { verifyRequest } from '@avada/shopify-auth';

const router = new Router({
  prefix: '/api'
});

router.use(verifyRequest());

router.get('/settings', settingController.getSetting);
router.put('/settings', settingController.updateSetting);
router.get('/notifications', notificationController.getNotifications);


export default router;
