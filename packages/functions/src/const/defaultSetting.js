const defaultSetting = {
  displayDuration: 3,
  firstDelay: 5,
  popsInterval: 2,
  maxPopsDisplay: 10,
  hideTimeAgo: false,
  truncateProductName: false,
  position: 'bottom-left',
  includedUrls: '',
  excludedUrls: '',
  allowShow: 'all'
}

export default defaultSetting;