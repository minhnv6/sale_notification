import moment from 'moment';

export const formatTime = (data) => {
  data.forEach(item => {
    item.timestamp = moment(new Date(item.timestamp._seconds * 1000 + item.timestamp._nanoseconds / 100000)).fromNow()
    return item;
  })
  return data;
}