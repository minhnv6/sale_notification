import App from 'koa';
import * as errorService from '../services/errorService';
import router from '../routes/webhook';
import cors from 'koa2-cors';

const api = new App();
api.use(cors());

api.proxy = true;
api.use(router.allowedMethods());
api.use(router.routes());

api.on('error', errorService.handleError);

export default api;
