import App from 'koa';
import 'isomorphic-fetch';
import { shopifyAuth } from '@avada/shopify-auth';
import shopifyConfig from '../config/shopify';
import render from 'koa-ejs';
import path from 'path';
import createErrorHandler from '../middleware/errorHandler';
import firebase from 'firebase-admin';
import * as errorService from '../services/errorService';
import api from "./api";
import { create as createDefaultSetting } from '../repositories/settingRepository';
import { getFirst as getShop } from '../repositories/shopRepository';
import { handleCreateNotifications } from '../services/notificationService';
import Shopify from 'shopify-api-node';
import { createWebhook, createScriptTag } from '../services/shopifyService';


if (firebase.apps.length === 0) {
  firebase.initializeApp();
}

// Initialize all demand configuration for an application
const app = new App();
app.proxy = true;

render(app, {
  cache: true,
  debug: false,
  layout: false,
  root: path.resolve(__dirname, '../../views'),
  viewExt: 'html'
});
app.use(createErrorHandler());

// Register all routes for the application
app.use(
  shopifyAuth({
    apiKey: shopifyConfig.apiKey,
    firebaseApiKey: shopifyConfig.firebaseApiKey,
    initialPlan: {
      features: {},
      id: 'free',
      name: 'Free plan',
      periodDays: 3650,
      price: 0,
      trialDays: 0
    },
    scopes: shopifyConfig.scopes,
    secret: shopifyConfig.secret,
    successRedirect: '/',
    afterInstall: async ctx => {
      try {
        const shopifyDomain = ctx.state.shopify.shop;
        // const accessToken = ctx.state.shopify.accessToken;
        const shop = await getShop(shopifyDomain);
        const shopify = new Shopify({
          shopName: shop.name,
          accessToken: shop.accessToken
        });

        await Promise.all([
          createDefaultSetting(shop.id, shopifyDomain),
          handleCreateNotifications(shopify, shop),
          createWebhook({
            shopify: shopify,
            topic: 'orders/create',
            address: 'https://6cc6-123-16-32-226.ap.ngrok.io/webhook/order/new'
          }),
          createScriptTag({
            shopify: shopify,
            event: 'onload',
            src: 'https://localhost:3000/scripttag/avada-sale-pop.min.js'
          })
        ])
      } catch (e) {
        console.error(e)
      }
    }
  }).routes()
);

// Handling all errors
api.on('error', errorService.handleError);

export default app;
