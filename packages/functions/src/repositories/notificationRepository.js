import { Firestore } from '@google-cloud/firestore';

const firestore = new Firestore();
const collection = firestore.collection('notifications');

export async function create({ shopId, shopifyDomain, data }) {
  console.log(data)
  await Promise.all(data.map((notification) => {
    return collection.add({ ...notification, shopId, shopifyDomain })
  }))
}


export async function getAll({ shopId = '', shopifyDomain = '', limit = 30 } = {}) {
  let query;
  if (shopId) {
    query = collection.where('shopId', '==', shopId).orderBy('timestamp', 'desc').limit(limit);
  }
  if (shopifyDomain) {
    query = collection.where('shopifyDomain', '==', shopifyDomain).orderBy('timestamp', 'desc').limit(limit);
  }
  const docs = await query.get();
  const data = docs.docs.map(doc => {
    return { id: doc.id, ...doc.data() };
  })
  return data;
}


export async function checkIfExist(shopId) {
  const created = await collection.where('shopId', '==', shopId).limit(1).get();
  if (created.docs[0]) {
    console.log('true');
    return true;
  }
  console.log('false');
  return false;
}



