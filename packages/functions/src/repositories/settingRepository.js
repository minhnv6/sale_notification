// import { getShopByShopAndUID } from '@avada/shopify-auth';
import { Firestore } from '@google-cloud/firestore';
import defaultSetting from '../const/defaultSetting';

const firestore = new Firestore();
const collection = firestore.collection('settings');

export async function create(shopId, shopifyDomain) {
  const shopSetting = await collection.where('shopId', '==', shopId).limit(1).get();
  if (shopSetting.docs[0]) {
    return;
  }
  await collection.add({ ...defaultSetting, shopId: shopId, shopifyDomain: shopifyDomain })
}

export async function getFirst({ shopId = '', shopifyDomain = '' } = {}) {
  let query;
  if (shopId) {
    query = collection.where('shopId', '==', shopId);
  }
  if (shopifyDomain) {
    query = collection.where('shopifyDomain', '==', shopifyDomain);
  }
  const docs = await query.get();
  const data = docs.docs[0];
  return {
    id: data.id,
    ...data.data()
  };
}

export async function update(shopId, data) {
  const shopSetting = await collection.where('shopId', '==', shopId).get();
  const doccumentId = shopSetting.docs[0].id;
  return collection.doc(doccumentId).update({ ...data, shopId: shopId });
}


