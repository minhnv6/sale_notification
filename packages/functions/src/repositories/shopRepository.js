import { Firestore } from '@google-cloud/firestore';
import formatDateFields from '../helpers/formatDateFields';

const firestore = new Firestore();
const collection = firestore.collection('shops');

export async function getFirst(shopifyDomain) {
  const res = await collection.where('shopifyDomain', '==', shopifyDomain).get();
  if (!res.docs[0]) {
    return null;
  }
  const data = res.docs[0];
  return {
    id: data.id,
    ...formatDateFields(data.data())
  }
}
