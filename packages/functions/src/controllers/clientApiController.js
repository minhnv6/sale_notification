import { getAll as getNotifications } from '../repositories/notificationRepository';
import { getFirst as getSetting } from '../repositories/settingRepository';
import { formatTime } from '../helpers/utills/formatDate';

export async function clientGetNotification(ctx) {
  try {
    const shopifyDomain = ctx.query.shopifyDomain;
    const settings = await getSetting({ shopifyDomain });
    const notificationsData = await getNotifications({ shopifyDomain, limit: settings.maxPopsDisplay });
    const notifications = formatTime(notificationsData);

    ctx.body = {
      success: true,
      notifications: notifications,
      settings: settings
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}


