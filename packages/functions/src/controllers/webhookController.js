import { getFirst as getShop } from "../repositories/shopRepository";
import { create as createNotifications } from "../repositories/notificationRepository";
import { getNotificationItems } from "../services/notificationService";
import Shopify from 'shopify-api-node';

export async function listenNewOrder(ctx) {
  try {
    const shopifyDomain = ctx.get('X-Shopify-Shop-Domain');
    const orderData = ctx.req.body;
    const shop = await getShop(shopifyDomain);
    const shopify = new Shopify({
      shopName: shop.name,
      accessToken: shop.accessToken
    });
    const notification = await getNotificationItems(shopify, [orderData]);
    await createNotifications({ shopId: shop.id, shopifyDomain, data: notification });

    return (ctx.body = {
      success: true
    });
  } catch (e) {
    console.error(e)
    return (ctx.body = {
      success: false
    });
  }
}

