import { getAll as getAllNotification } from '../repositories/notificationRepository';
import { getCurrentUserInstance } from '../helpers/auth';

export async function getNotifications(ctx) {
  try {
    const { shopID } = getCurrentUserInstance(ctx);
    const notifications = await getAllNotification({ shopId: shopID });

    ctx.body = {
      success: true,
      data: notifications
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}


