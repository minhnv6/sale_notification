import { getFirst as getSetting } from '../repositories/settingRepository';
import { getCurrentUserInstance } from '../helpers/auth';

export async function getSetting(ctx) {
  try {
    const { shopID } = getCurrentUserInstance(ctx);
    const settings = await getSetting({ shopId: shopID });

    ctx.body = {
      success: true,
      data: settings
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}

export async function updateSetting(ctx) {
  try {
    const { shopID } = getCurrentUserInstance(ctx);
    const postData = ctx.req.body;
    await settingRepository.update(shopID, postData);

    ctx.status = 200;
    return ctx.body = {
      success: true
    }
  } catch (e) {
    return ctx.body = {
      success: false,
      error: e.message
    }
  }
}



