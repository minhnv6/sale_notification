/**
 * 
 * @param {*} shopify 
 * @returns 
 */
export async function getOrders(shopify) {
  const orders = await shopify.order.list({ limit: 30 });
  return orders;
}

/**
 * 
 * @param {*} shopify 
 * @param {*} listIdProduct 
 * @returns 
 */
export async function getProducts(shopify, listIdProduct) {
  const products = await shopify.product.list({
    ids: listIdProduct.toString()
  });
  return products;
}

/**
 * 
 * @param {*} param0 
 * @returns 
 */
export async function createWebhook({ shopify, topic, address } = {}) {
  return shopify.webhook.create({
    topic: topic,
    address: address
  })
}

/**
 * 
 * @param {*} param0 
 * @returns 
 */
export async function createScriptTag({ shopify, event, src } = {}) {
  const created = await shopify.scriptTag.list({ limit: 1 });
  if (created[0]) {
    return;
  }

  return shopify.scriptTag.create({
    event: event,
    src: src
  })
}