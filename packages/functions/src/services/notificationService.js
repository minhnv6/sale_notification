import { getOrders, getProducts } from './shopifyService';
import { create as createNotifications, checkIfExist } from '../repositories/notificationRepository';
import onlyUnique from '../helpers/utills/onlyUnique';

export async function handleCreateNotifications(shopify, shop) {
  if (await checkIfExist(shop.id)) return;
  const orders = await getOrders(shopify);
  const notifications = await getNotificationItems(shopify, orders);
  await createNotifications({ shopId: shop.id, shopifyDomain: shop.shopifyDomain, data: notifications });
}

export async function getNotificationItems(shopify, orderData) {
  const listProductId = orderData.map((order) => order.line_items[0].product_id);
  const productIds = listProductId.filter(onlyUnique);
  const products = await getProducts(shopify, productIds);

  const notifications = orderData.map((order) => {
    const { billing_address, line_items, created_at } = order;
    const product = products.find((product) => product.id === line_items[0].product_id);
    return {
      firstName: billing_address.first_name,
      city: billing_address.city,
      productName: product.title,
      country: billing_address.country,
      productId: product.id,
      timestamp: new Date(created_at),
      productImage: product.image.src,
    }
  })

  return notifications;
};




